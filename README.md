# Reign HACKER NEWS

Tech test project for the Front End Developer offer.

The app request data to the [Hackers News public API](https://hn.algolia.com/api). which considers the following functionalities:

- List the posts requested from the API with infinite scroll pagination
- Filters and favorited posts saves on the local storage
- Responsiveness
- Open post url in a new tab when card is clicked
- Like button to save a favorite post
- Opacity style when card is hovering

[Live Demo](https://effervescent-starship-5012f6.netlify.app/)
