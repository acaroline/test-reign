export const urlLastStories = 'https://hn.algolia.com/api/v1/search_by_date?tags=story'
export const urlAngular = (page: number) => `https://hn.algolia.com/api/v1/search_by_date?query=angular&page=${page-1}`
export const urlReactjs = (page: number) => `https://hn.algolia.com/api/v1/search_by_date?query=reactjs&page=${page-1}`
export const urlVuejs = (page: number) => `https://hn.algolia.com/api/v1/search_by_date?query=vuejs&page=${page-1}`
