import {
  urlAngular,
  urlLastStories,
  urlReactjs,
  urlVuejs,
} from "../../api/urls"
import { FilterTypes } from "./types"

export const getUrlFilter = (page: number, filter: FilterTypes) => {
  const FILTER = {
    all: urlLastStories,
    angular: urlAngular(page),
    reactjs: urlReactjs(page),
    vuejs: urlVuejs(page),
  }

  return FILTER[filter]
}
