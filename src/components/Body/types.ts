export type Hit = {
  author: string
  created_at: string
  objectID: string
  story_title: string
  story_url: string | null
  title: string
  url: string | null
}

export type Post = {
  hits: Hit[]
  hitsPerPage: number
  nbHits: number
  nbPages: number
  page: number
  params: string
  processingTimeMS: number
  query: string
}

export type FilterTypes = 'all' | 'angular' | 'reactjs' | 'vuejs'
