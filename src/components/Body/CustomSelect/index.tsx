import { Select } from "antd"
import { FilterTypes } from "../types"
import { filterOptions, FilterOptionsProps } from "./constants"
import './CustomSelect.css'

const { Option } = Select

type CustomSelectProps = {
  setFilter: React.Dispatch<React.SetStateAction<FilterTypes>>
  setPage: React.Dispatch<React.SetStateAction<number>>
}

const CustomSelect: React.FC<CustomSelectProps> = ({ setFilter, setPage }) => {
  const handleChange = (value: string) => {
    localStorage.setItem('filter', value)
    setFilter(value as FilterTypes)
    setPage(1)
  }

  return (
    <Select className="Dropdown" defaultValue={localStorage.getItem('filter') ?? 'all'} onChange={handleChange}>
      <Option value="all" label="Angular">Select your news</Option>
      {filterOptions.map(op => (
        <Option key={op.value} value={op.value}>
          <CustomOption label={op.label} icon={op.icon} />
        </Option>
      ))}
    </Select>
  )
}

const CustomOption: React.FC<Omit<FilterOptionsProps, 'value'>> = ({ label, icon }) => {
  return (
    <>
      {icon}
      <span className="ml10">{label}</span>
    </>
  )
}

export default CustomSelect
