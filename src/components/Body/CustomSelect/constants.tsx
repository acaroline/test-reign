import { AngularIcon } from "../../../assets/AngularIcon"
import { ReactIcon } from "../../../assets/ReactIcon"
import { VueIcon } from "../../../assets/VueIcon"
import { FilterTypes } from "../types"

export type FilterOptionsProps = {
  label: string
  value: FilterTypes
  icon: JSX.Element
}

export const filterOptions: FilterOptionsProps[] = [
  { label: "Angular", value: "angular", icon: <AngularIcon /> },
  { label: "Reactjs", value: "reactjs", icon: <ReactIcon /> },
  { label: "Vuejs", value: "vuejs", icon: <VueIcon /> },
]
