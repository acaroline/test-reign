import { Col, Row } from "antd"
import { useEffect, useState } from "react"
import { apiGet } from "../../api"
import { FilterTypes, Hit, Post } from "./types"
import PostsList from "./PostsList"
import './Body.css'
import CustomSelect from "./CustomSelect"
import { getUrlFilter } from "./constants"
import { PostType } from "../../types"
import { useInView } from "react-cool-inview"

type BodyProps = { postType: PostType }

const Body: React.FC<BodyProps> = ({ postType }) => {
  const [post, setPost] = useState<Post>()
  const [page, setPage] = useState<number>(1)
  const [filter, setFilter] = useState<FilterTypes>((localStorage.getItem('filter') ?? 'all') as FilterTypes)
  const [favPosts, setFavPosts] = useState<Hit[]>((JSON.parse(localStorage.getItem('favPosts') ?? '[]')) as Hit[])

  useEffect(() => {
    const getPosts = async () => {
      const url = getUrlFilter(page, filter)
      const newData = await apiGet(url)

      setPost(prev => {
        if (prev?.page === newData.page && prev?.query === filter)
          return prev

        if (prev && prev.query === filter)
          return { ...newData, hits: [...prev.hits, ...newData.hits] }
        return newData
      })
    }

    getPosts()
  }, [filter, page])

  const { observe } = useInView({
    rootMargin: "50px 0px",
    onEnter: async ({ unobserve }) => {
      unobserve()
      setPage(prev => prev + 1)
    },
  })

  return (
    <div className="Container">
      <Row className="mb38">
        {postType === 'all' && (
          <Col span={24}>
            <CustomSelect setFilter={setFilter} setPage={setPage} />
          </Col>
        )}
      </Row>
      <Row gutter={[40, 30]}>
        <PostsList
          postType={postType}
          post={post}
          favPosts={favPosts}
          setFavPosts={setFavPosts}
          observe={observe} />
      </Row>
      <Row className="mt96 pb98" justify="center">
      </Row>
    </div>
  )
}

export default Body
