import { Col, Typography } from "antd"
import CustomCard from "../CustomCard"
import { Hit, Post } from "../types"

type PostsListProps = {
  postType: string
  post: Post | undefined
  favPosts: Hit[]
  setFavPosts: React.Dispatch<React.SetStateAction<Hit[]>>
  observe: (element?: HTMLElement | null | undefined) => void
}

const PostsList: React.FC<PostsListProps> = ({ postType, post, favPosts, setFavPosts, observe }) => {
  if (postType === 'all')
    return (
      <>
        {post?.hits.map((hit, index) => (
          <Col xs={24} md={12} key={hit.objectID} ref={index === post?.hits.length - 1 ? observe : null}>
            <CustomCard
              data={hit}
              isFav={favPosts.some(favPost => favPost.objectID === hit.objectID)}
              setFavPosts={setFavPosts} />
          </Col>
        ))}
      </>
    )
  else
    return (
      <>
        {favPosts.length === 0 ? (
          <Col span={24} className='Text-Center'>
            <Typography.Title level={4}>No data to display</Typography.Title>
          </Col>
        ) : (
          favPosts.map((hit) => (
            <Col xs={24} md={12} key={hit.objectID}>
              <CustomCard
                data={hit}
                isFav={favPosts.some(favPost => favPost.objectID === hit.objectID)}
                setFavPosts={setFavPosts} />
            </Col>
          ))
        )}
      </>
    )
}

export default PostsList
