import { Card, Col, message, Row } from "antd"
import { ClockCircleOutlined, HeartTwoTone, HeartFilled } from '@ant-design/icons'
import './CustomCard.css'
import { Hit } from "../types"
import RelativeTime from '@yaireo/relative-time'

type CustomCardProps = {
  data: Hit
  isFav: boolean
  setFavPosts: React.Dispatch<React.SetStateAction<Hit[]>>
}

const CustomCard: React.FC<CustomCardProps> = ({ data, isFav, setFavPosts }) => {
  const relativeTimeSpanish = new RelativeTime()

  const onClickFav = (ev: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    ev.stopPropagation()

    const favPosts = (JSON.parse(localStorage.getItem('favPosts') ?? '[]')) as Hit[]
    const favPostIndex = favPosts.findIndex(favPost => favPost.objectID === data.objectID)

    if (favPostIndex !== -1)
      favPosts.splice(favPostIndex, 1)
    else
      favPosts.push(data)

    setFavPosts(favPosts)
    localStorage.setItem('favPosts', JSON.stringify(favPosts))
  }

  const onClickOpenUrl = () => {
    const url = data.story_url ?? data.url
    if (url) window.open(url, '_blank')
    else message.info('No url available')
  }

  return (
    <Card className="Card" onClick={onClickOpenUrl}>
      <Row align="middle">
        <Col xs={24} lg={20} className="p24">
          <Row align="middle" className="mb6">
            <ClockCircleOutlined className="Clock-Icon mr8" />
            <span className="Small-Text">
              {relativeTimeSpanish.from(new Date(data.created_at))} by {data.author}
            </span>
          </Row>
          <span className="Bold-Text">{data.story_title ?? data.title}</span>
        </Col>
        <Col xs={24} lg={4} className="Col" onClick={onClickFav}>
          {isFav ? (
            <HeartFilled className="Fav-Icon Filled-Icon" />
          ) : (
            <HeartTwoTone className="Fav-Icon" twoToneColor="#DD0031" />
          )}
        </Col>
      </Row>
    </Card>
  )
}

export default CustomCard
