import './Title.css'

const Title: React.FC = () => (
  <span className="HACKER-NEWS">
    HACKER NEWS
  </span>
)

export default Title
