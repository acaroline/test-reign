import { Col, Radio, RadioChangeEvent, Row } from "antd"
import Title from "./Title"

import './Header.css'
import { PostType } from "../../types"

type HeaderProps = {
  setPostType: React.Dispatch<React.SetStateAction<PostType>>
}

const Header: React.FC<HeaderProps> = ({ setPostType }) => {
  const onChange = (e: RadioChangeEvent) => {
    setPostType(e.target.value)
    localStorage.setItem('postsType', e.target.value)
  }

  return (
    <div>
      <Row className="Header-Container" align="middle">
        <Col span={24}>
          <Title />
        </Col>
      </Row>
      <Row justify="center">
        <Col>
          <Radio.Group className="Radio-Group" onChange={onChange} defaultValue={localStorage.getItem('postsType') ?? 'all'}>
            <Radio.Button value="all" className="Radio-Button">All</Radio.Button>
            <Radio.Button value="favs" className="Radio-Button">My favs</Radio.Button>
          </Radio.Group>
        </Col>
      </Row>
    </div>
  )
}

export default Header
