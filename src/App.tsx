import { useState } from 'react';
import { PostType } from './types';
import './App.css';
import Body from './components/Body';
import Header from './components/Header';

function App() {
  const [postType, setPostType] = useState<'all' | 'favs'>((localStorage.getItem('postsType') ?? 'all') as PostType)

  return (
    <div className="App">
      <Header setPostType={setPostType} />
      <Body postType={postType} />
    </div>
  );
}

export default App;
